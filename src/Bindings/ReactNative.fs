module Fable.ReactNative

open Fable.Core
open Fable.Core.JsInterop

module Types =
    [<StringEnum>]
    type ColorScheme =
        | Light
        | Dark

    type Appearance =
        abstract getColorScheme : unit -> ColorScheme
        abstract addChangeListener : ({| colorScheme: ColorScheme |} -> unit) -> unit

[<Erase>]
module RN =
    [<Import("Appearance", "react-native")>]
    let Appearance : Types.Appearance = jsNative