module AsyncStorage

open Fable
open Fable.Core

type AsyncStorage =
    abstract getItem: string -> JS.Promise<string>
    abstract setItem: string -> string -> JS.Promise<string>