module LinearGradient

open Fable.Core
open Fable.Core.JsInterop

open Fable.React
open Fable.ReactNative.Props

type Vector2 = { x: int; y: int }

type ILinearGradientProps =
    interface
    end

type LinearGradientProps =
    | Colors of string array
    | Start of Vector2
    | End of Vector2
    interface ILinearGradientProps

    static member Style(style: IStyle list) : ILinearGradientProps =
        !!("style", keyValueList CaseRules.LowerFirst style)


let linearGradient (props: ILinearGradientProps list) (elements: ReactElement list) : ReactElement =
    ofImport "default" "react-native-linear-gradient" (keyValueList CaseRules.LowerFirst props) elements
