module Sounds

open Fable.Core
open Fable.Core.JsInterop
open SoundBinding

type ButtonPosition =
    | Left
    | Middle
    | Right

type SoundObject =
    { Audio: Sound
      Name: string
      mutable IsPlaying: bool
      Image: Fable.ReactNative.Props.IImageSource
      Position: ButtonPosition }


[<ImportDefault("react-native-sound")>]
let sound: Sound = jsNative


let dejaVu =
    { Audio = sound.createSound "deja_vu.mp3"
      Name = "dejaVu"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/logo-sncf.png")
      Position = Middle }

let sncf =
    { Audio = sound.createSound "sncf.mp3"
      Name = "sncf"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/logo-sncf.png")
      Position = Left }

let ah =
    { Audio = sound.createSound "ah.mp3"
      Name = "ah"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/ah.png")
      Position = Middle }

let toca =
    { Audio = sound.createSound "tocata.mp3"
      Name = "toca"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/minions.png")
      Position = Right }

let tac =
    { Audio = sound.createSound "tac.mp3"
      Name = "tac"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/clav.png")
      Position = Left }

let bull =
    { Audio = sound.createSound "bull.mp3"
      Name = "bull"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/bulle.png")
      Position = Middle }

let sonnette =
    { Audio = sound.createSound "sonette.mp3"
      Name = "sonnette"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/sonnette.png")
      Position = Right }

let tombo =
    { Audio = sound.createSound "tombo.mp3"
      Name = "tombo"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/tambour.png")
      Position = Left }

let bol =
    { Audio = sound.createSound "bol.mp3"
      Name = "bol"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/bol.png")
      Position = Middle }

let marche =
    { Audio = sound.createSound "marche.mp3"
      Name = "marche"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/marche.png")
      Position = Right }

let av =
    { Audio = sound.createSound "avengers.mp3"
      Name = "av"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/avengers.png")
      Position = Left }

let non =
    { Audio = sound.createSound "gotaga.mp3"
      Name = "non"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/non.png")
      Position = Middle }

let coucou =
    { Audio = sound.createSound "coucou.mp3"
      Name = "coucou"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/coucou.png")
      Position = Right }

let temp =
    { Audio = sound.createSound "tempate.mp3"
      Name = "temp"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/temp.png")
      Position = Left }

let rob =
    { Audio = sound.createSound "roblox.mp3"
      Name = "rob"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/roblox.png")
      Position = Middle }

let prout =
    { Audio = sound.createSound "prout.mp3"
      Name = "prout"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/prout.png")
      Position = Right }

let gifi =
    { Audio = sound.createSound "gifi.mp3"
      Name = "gifi"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/gifi.png")
      Position = Left }

let sat =
    { Audio = sound.createSound "saturer.mp3"
      Name = "sat"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/sat.png")
      Position = Middle }

let swing =
    { Audio = sound.createSound "swing.mp3"
      Name = "swing"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/elec.png")
      Position = Right }

let death =
    { Audio = sound.createSound "death.mp3"
      Name = "death"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/death.png")
      Position = Left }

let popo =
    { Audio = sound.createSound "popo.mp3"
      Name = "popo"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/médoc-liquide.png")
      Position = Middle }

let shot =
    { Audio = sound.createSound "headshot.mp3"
      Name = "shot"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/headshot.png")
      Position = Right }

let verre =
    { Audio = sound.createSound "verre.mp3"
      Name = "verre"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/verre.png")
      Position = Left }

let goute =
    { Audio = sound.createSound "eau.mp3"
      Name = "goute"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/goute.png")
      Position = Middle }

let coq =
    { Audio = sound.createSound "coq.mp3"
      Name = "coq"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/coq.png")
      Position = Right }

let poule =
    { Audio = sound.createSound "poule.mp3"
      Name = "poule"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/poulet.png")
      Position = Left }

let fox =
    { Audio = sound.createSound "twentyth_century_fox.mp3"
      Name = "fox"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/20th_century_fox.png")
      Position = Middle }

let engine =
    { Audio = sound.createSound "engine.mp3"
      Name = "engine"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/ferrari.png")
      Position = Right }

let universal =
    { Audio = sound.createSound "universal_studios.mp3"
      Name = "universal"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/universal.png")
      Position = Left }

let mario =
    { Audio = sound.createSound "mario.mp3"
      Name = "mario"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/level.png")
      Position = Middle }

let piece =
    { Audio = sound.createSound "piece.mp3"
      Name = "piece"
      IsPlaying = false
      Image = (Fable.ReactNative.Helpers.localImage "../assets/SoundBox/Images/piece.png")
      Position = Right }

let allSoundPacks =
    [ [ sncf; ah; toca ]
      [ tac; bull; sonnette ]
      [ tombo; bol; marche ]
      [ av; non; coucou ]
      [ temp; rob; prout ]
      [ gifi; sat; swing ]
      [ death; popo; shot ]
      [ verre; goute; coq ]
      [ poule; fox; engine ]
      [ universal; mario; piece ] ]


let playOrStopSound (sound: SoundObject) =
    if not sound.IsPlaying then
        sound.IsPlaying <- true
        sound.Audio.play(fun _ ->
            sound.IsPlaying <- false
            printfn "%s finished" sound.Name
        )

        printfn "%s is playing" sound.Name
    else
        sound.IsPlaying <- false
        sound.Audio.stop()
        printfn "%s stopped" sound.Name