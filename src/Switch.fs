module Switch

open Fable.Core
open Fable.Core.JsInterop
open Fable.React
open Fable.ReactNative.Props

open ReactAnimation
open LinearGradient

let AnimatedView (props: IViewProperties list) (elements: ReactElement list) : ReactElement =
    ofImport "Animated.View" "react-native" (keyValueList CaseRules.LowerFirst props) elements


type SwitchProps =
    | Value of bool
    | OnPress of (bool -> unit)

let position = Animated.Value 0.

let switchAnimation value =
    Animated.timing(position, { toValue = (if value then 30. else 0.); duration = 100; useNativeDriver = true }).start()

let setSwitchOn() =
    Animated.timing(position, { toValue = 30.; duration = 0; useNativeDriver = true }).start()

module R = Fable.ReactNative.Helpers
module P = Fable.ReactNative.Props
open Fable.ReactNative.Props

let switch (isOn: bool) (style: IStyle list) (onPress: unit -> unit) =
    switchAnimation isOn

    R.touchableWithoutFeedback [
        OnPress onPress

        HitSlop {| bottom = 15.; left = 15.; right = 15.; top = 15.; |}
    ] [
        linearGradient [ //? Background
            LinearGradientProps.Style ([
                Width (R.dip 60.)
                Height (R.dip 30.)
                if isOn then
                    BackgroundColor "#ffffff"
                else
                    BackgroundColor "#141C1F"
                BorderRadius 100.

                P.FlexStyle.JustifyContent JustifyContent.Center

                Elevation 8.
            ] @ style)

            LinearGradientProps.Colors [| "#64DFDF"; "#48BFE3" |]

            LinearGradientProps.Start { x = 0; y = 0 }
            LinearGradientProps.End { x = 1; y = 1 }
        ] [
            AnimatedView [ //? Ball
                P.ViewProperties.Style [

                    P.FlexStyle.Left (R.dip 0.)
                    P.TranslateX position

                    Width (R.dip 25.)
                    Height (R.dip 25.)

                    Margin (R.dip 2.5)

                    if isOn then
                        BackgroundColor "#ffffff"
                    else
                        BackgroundColor "#141C1F"

                    Elevation 4.
                    BorderRadius 100.
                ]
            ] []
        ]
    ]