/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './out/App';
import {name as appName} from './app.json';